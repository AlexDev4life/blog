(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('LoginController', LoginController);

		LoginController.$inject = ['$scope', '$rootScope', 'ngDialog', 'User', '$window'];

		function LoginController($scope, $rootScope, ngDialog, User, $window){
			var vm = this; 
			var dialog;

			vm.login = login;
			vm.logout = logout;
			vm.showLogin = showLogin;
			vm.showRegister = showRegister;
			vm.signIn = signIn;
			vm.displayLogin = true;
			vm.register = false;
			vm.error = false;

			getUsers();

			function login(){
				vm.displayLogin = true;
				vm.log = {};
				vm.error = false;
				dialog = ngDialog.open({
	                  template: 'views/auth/login.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function logout(){
				localStorage.removeItem('user');
				$rootScope.userPosts = [];
				$window.location.href = '#home';
			}

			function signIn(){
				var user = '';
				if(vm.login != undefined){
					user = vm.users.filter(x => x.username == vm.log.username)[0];
				}			
				if(user != undefined){
					vm.error = false;
					dialog.close();
					localStorage.setItem('user', JSON.stringify(user));			
					$window.location.href = '#home';
				}
				else{
					vm.error = true;
					vm.login = {};
				}	
			}

			function showLogin(){
				if(!vm.login || vm.login == undefined)
					vm.login = true;
				vm.register = false;
			}

			function showRegister(){
				if(!vm.register || vm.register == undefined)
					vm.register = true;
				vm.login = false;
			}

			function getUsers(){
				User.query(function(users){
					vm.users = users;
				})
			}
			
		}

})();