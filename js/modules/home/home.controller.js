(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('HomeController', HomeController);

		HomeController.$inject = ['$scope', 'Post', 'User', 'Comment', 'ngDialog', '$rootScope', '$window'];

		function HomeController($scope, Post, User, Comment, ngDialog, $rootScope, $window){
			var vm = this;
			var dialog; 
			var dummy = {};
			var data = [];

			vm.userDetails = userDetails
			vm.olderPosts = olderPosts;
			vm.postInfo = postInfo;
			vm.register = false;
			vm.posts = [];
			$rootScope.userPosts = undefined;

			getPosts();
			
			function checkSession(){
				if(localStorage.getItem('user') != undefined){
					var userId = JSON.parse(localStorage.getItem('user')).id;
					var array; 
					$rootScope.userPosts = vm.posts.filter(x => x.userId == userId);
				}
			}
	

			function userDetails(id){
				vm.user = vm.users.filter(x => x.id == id)[0];
				dialog = ngDialog.open({
	                  template: 'views/user/user.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function postInfo(id){
				vm.post = data.filter(x => x.id == id)[0];
				dialog = ngDialog.open({
	                  template: 'views/post/post.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function olderPosts(){
				var array; 
				if(vm.data.length + 5 < data.length){
					array = data.slice(vm.data.length, vm.data.length + 5);
					vm.data = vm.data.concat(array);
				}
			}

			function getPosts(){
				Post.query(function(posts){
					vm.posts = posts;
					checkSession();
					getComments();
				})
			}

			function getComments(){
				Comment.query(function(comments){
					vm.comments = comments;
					getUsers();
				})
			}

			function getUsers(){
				User.query(function(users){
					vm.users = users;
					buildData();
				})
			}

			function buildData(){
				var userIds = vm.posts.map(x => x.userId);

				vm.posts.map(function(post, index){
					if(!dummy.hasOwnProperty(post.id)){
						dummy[post.id] = {
							id: '',
							userId: '',
							title: '',
							body: '',
							username: '',
							author: '',
							date: new Date().toLocaleDateString(),
							commentCount: 0
						};

					}

					dummy[post.id].id = post.id;
					dummy[post.id].title = post.title;
					dummy[post.id].body = post.body;

					vm.comments.map(function(comment, index){
						if(comment.postId == post.id){
							dummy[post.id].commentCount += 1;
						}
					});

					vm.users.map(function(user, index){
						if(user.id == post.userId){
							dummy[post.id].username = user.username;
							dummy[post.id].author = user.name;
							dummy[post.id].userId = user.id;
						}
					});
				});
				Object.keys(dummy).map(function(obj, index){
					data.push(dummy[obj]);
				});

				vm.data = data.slice(0, 5);
			}
		}

})();