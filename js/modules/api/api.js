(function(){
	'use strict';

	var urlBase = 'https://jsonplaceholder.typicode.com';

	angular
		.module('lbServices', ['ngResource'])
		.factory('Post', Post)
		.factory('Comment', Comment)
		.factory('User', User);

		function Post($resource){
			return $resource(urlBase + '/posts/:id/:userId', 
				{
				  id: '@id',
				  userId: '@userId' 
				},
				{
				   'comments':
				   {
				   		isArray: true,
				   		url: urlBase + '/posts/:id/comments',
				   		method: 'GET',
				   }
				}
			);
		}

		function Comment($resource) {
			return $resource(urlBase + '/comments/:postId', {postId: '@postId'},
			{
				'update': {method: 'PUT'}
			});
		}

		function User($resource){
			return $resource(urlBase + '/users',
			{
				'update': {method: 'PUT'}
			});
		}

})();