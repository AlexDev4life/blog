(function(){
	'use strict';
	angular
		.module('blog-app', ['ngRoute', 'lbServices', 'ngDialog'])
		.config(function($routeProvider){

			$routeProvider
				.when('/home',{
					templateUrl: 'views/home/home.html',
					controller: 'HomeController',
					controllerAs: 'hc'
				})
				.when('/about',{
					templateUrl: 'views/about/about.html',
					controller: 'AboutController',
					controllerAs: 'ac'
				})
				.when('/post',{
					templateUrl: 'views/post/post.html',
					controller: 'PostController',
					controllerAs: 'pc'
				})
				.when('/contact',{
					templateUrl: 'views/contact/contact.html',
					controller: 'ContactController',
					controllerAs: 'cc'
				})
				.when('/login',{
					templateUrl: 'views/auth/login.html',
					controller: 'LoginController',
					controllerAs: 'lc'
				})
				.when('/user_posts',{
					templateUrl: 'views/user_posts/user_posts.html',
					controller: 'UserPostsController',
					controllerAs: 'upc'
				})
				.otherwise({
					redirectTo: '/home'
				});
		});
})();