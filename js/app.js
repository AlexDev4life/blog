(function(){
	'use strict';
	angular
		.module('blog-app', ['ngRoute', 'lbServices', 'ngDialog'])
		.config(function($routeProvider){

			$routeProvider
				.when('/home',{
					templateUrl: 'views/home/home.html',
					controller: 'HomeController',
					controllerAs: 'hc'
				})
				.when('/about',{
					templateUrl: 'views/about/about.html',
					controller: 'AboutController',
					controllerAs: 'ac'
				})
				.when('/post',{
					templateUrl: 'views/post/post.html',
					controller: 'PostController',
					controllerAs: 'pc'
				})
				.when('/contact',{
					templateUrl: 'views/contact/contact.html',
					controller: 'ContactController',
					controllerAs: 'cc'
				})
				.when('/login',{
					templateUrl: 'views/auth/login.html',
					controller: 'LoginController',
					controllerAs: 'lc'
				})
				.when('/user_posts',{
					templateUrl: 'views/user_posts/user_posts.html',
					controller: 'UserPostsController',
					controllerAs: 'upc'
				})
				.otherwise({
					redirectTo: '/home'
				});
		});
})();
(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('AboutController', AboutController);

		AboutController.$inject = ['$scope', 'Post'];

		function AboutController($scope, Post){
			
		}

})();
(function(){
	'use strict';

	var urlBase = 'https://jsonplaceholder.typicode.com';

	angular
		.module('lbServices', ['ngResource'])
		.factory('Post', Post)
		.factory('Comment', Comment)
		.factory('User', User);

		function Post($resource){
			return $resource(urlBase + '/posts/:id/:userId', 
				{
				  id: '@id',
				  userId: '@userId' 
				},
				{
				   'comments':
				   {
				   		isArray: true,
				   		url: urlBase + '/posts/:id/comments',
				   		method: 'GET',
				   }
				}
			);
		}

		function Comment($resource) {
			return $resource(urlBase + '/comments/:postId', {postId: '@postId'},
			{
				'update': {method: 'PUT'}
			});
		}

		function User($resource){
			return $resource(urlBase + '/users',
			{
				'update': {method: 'PUT'}
			});
		}

})();
(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('LoginController', LoginController);

		LoginController.$inject = ['$scope', '$rootScope', 'ngDialog', 'User', '$window'];

		function LoginController($scope, $rootScope, ngDialog, User, $window){
			var vm = this; 
			var dialog;

			vm.login = login;
			vm.logout = logout;
			vm.showLogin = showLogin;
			vm.showRegister = showRegister;
			vm.signIn = signIn;
			vm.displayLogin = true;
			vm.register = false;
			vm.error = false;

			getUsers();

			function login(){
				vm.displayLogin = true;
				vm.log = {};
				vm.error = false;
				dialog = ngDialog.open({
	                  template: 'views/auth/login.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function logout(){
				localStorage.removeItem('user');
				$rootScope.userPosts = [];
				$window.location.href = '#home';
			}

			function signIn(){
				var user = '';
				if(vm.login != undefined){
					user = vm.users.filter(x => x.username == vm.log.username)[0];
				}			
				if(user != undefined){
					vm.error = false;
					dialog.close();
					localStorage.setItem('user', JSON.stringify(user));			
					$window.location.href = '#home';
				}
				else{
					vm.error = true;
					vm.login = {};
				}	
			}

			function showLogin(){
				if(!vm.login || vm.login == undefined)
					vm.login = true;
				vm.register = false;
			}

			function showRegister(){
				if(!vm.register || vm.register == undefined)
					vm.register = true;
				vm.login = false;
			}

			function getUsers(){
				User.query(function(users){
					vm.users = users;
				})
			}
			
		}

})();
(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('ContactController', ContactController);

		ContactController.$inject = ['$scope', 'Post'];

		function ContactController($scope, Post){
			var vm = this;
			
		}

})();
(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('HomeController', HomeController);

		HomeController.$inject = ['$scope', 'Post', 'User', 'Comment', 'ngDialog', '$rootScope', '$window'];

		function HomeController($scope, Post, User, Comment, ngDialog, $rootScope, $window){
			var vm = this;
			var dialog; 
			var dummy = {};
			var data = [];

			vm.userDetails = userDetails
			vm.olderPosts = olderPosts;
			vm.postInfo = postInfo;
			vm.register = false;
			vm.posts = [];
			$rootScope.userPosts = undefined;

			getPosts();
			
			function checkSession(){
				if(localStorage.getItem('user') != undefined){
					var userId = JSON.parse(localStorage.getItem('user')).id;
					var array; 
					$rootScope.userPosts = vm.posts.filter(x => x.userId == userId);
				}
			}
	

			function userDetails(id){
				vm.user = vm.users.filter(x => x.id == id)[0];
				dialog = ngDialog.open({
	                  template: 'views/user/user.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function postInfo(id){
				vm.post = data.filter(x => x.id == id)[0];
				dialog = ngDialog.open({
	                  template: 'views/post/post.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function olderPosts(){
				var array; 
				if(vm.data.length + 5 < data.length){
					array = data.slice(vm.data.length, vm.data.length + 5);
					vm.data = vm.data.concat(array);
				}
			}

			function getPosts(){
				Post.query(function(posts){
					vm.posts = posts;
					checkSession();
					getComments();
				})
			}

			function getComments(){
				Comment.query(function(comments){
					vm.comments = comments;
					getUsers();
				})
			}

			function getUsers(){
				User.query(function(users){
					vm.users = users;
					buildData();
				})
			}

			function buildData(){
				var userIds = vm.posts.map(x => x.userId);

				vm.posts.map(function(post, index){
					if(!dummy.hasOwnProperty(post.id)){
						dummy[post.id] = {
							id: '',
							userId: '',
							title: '',
							body: '',
							username: '',
							author: '',
							date: new Date().toLocaleDateString(),
							commentCount: 0
						};

					}

					dummy[post.id].id = post.id;
					dummy[post.id].title = post.title;
					dummy[post.id].body = post.body;

					vm.comments.map(function(comment, index){
						if(comment.postId == post.id){
							dummy[post.id].commentCount += 1;
						}
					});

					vm.users.map(function(user, index){
						if(user.id == post.userId){
							dummy[post.id].username = user.username;
							dummy[post.id].author = user.name;
							dummy[post.id].userId = user.id;
						}
					});
				});
				Object.keys(dummy).map(function(obj, index){
					data.push(dummy[obj]);
				});

				vm.data = data.slice(0, 5);
			}
		}

})();
(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('PostController', PostController);

		PostController.$inject = ['$scope', 'Post'];

		function PostController($scope, Post){
			
		}

})();
(function(){
	'use strict';
	angular
		.module('blog-app')
		.controller('UserPostsController', UserPostsController);

		UserPostsController.$inject = ['$scope', 'Post', 'User', 'Comment', 'ngDialog', '$rootScope', '$window'];

		function UserPostsController($scope, Post, User, Comment, ngDialog, $rootScope, $window){
			var vm = this;
			var dialog; 
			var dummy = {};
			var data = [];

			vm.postInfo = postInfo;
			vm.posts = [];
			$rootScope.userPosts = true;

			getPosts();
			
			function checkSession(){
				if(localStorage.getItem('user') != undefined){
					var userId = JSON.parse(localStorage.getItem('user')).id;
					vm.data = vm.posts.filter(x => x.userId == userId).map(function(post){
						post.title = post.title.slice(0, 10);
						post.body = post.body.slice(0, 30);
					});
				}
			}
	
			function postInfo(id){
				vm.post = data.filter(x => x.id == id)[0];
				dialog = ngDialog.open({
	                  template: 'views/post/post.html',
	                  className: 'ngdialog-theme-default',
	                  scope: $scope
                });
			}

			function getPosts(){
				Post.query(function(posts){
					vm.posts = posts;
					checkSession();
					getComments();
				})
			}

			function getComments(){
				Comment.query(function(comments){
					vm.comments = comments;
					getUsers();
				})
			}

			function getUsers(){
				User.query(function(users){
					vm.users = users;
					buildData();
				})
			}

			function buildData(){
				var userIds = vm.posts.map(x => x.userId);

				vm.posts.map(function(post, index){
					if(!dummy.hasOwnProperty(post.id)){
						dummy[post.id] = {
							id: '',
							userId: '',
							title: '',
							body: '',
							username: '',
							author: '',
							date: new Date().toLocaleDateString(),
							commentCount: 0
						};

					}

					dummy[post.id].id = post.id;
					dummy[post.id].title = post.title;
					dummy[post.id].body = post.body;

					vm.comments.map(function(comment, index){
						if(comment.postId == post.id){
							dummy[post.id].commentCount += 1;
						}
					});

					vm.users.map(function(user, index){
						if(user.id == post.userId){
							dummy[post.id].username = user.username;
							dummy[post.id].author = user.name;
							dummy[post.id].userId = user.id;
						}
					});
				});
				Object.keys(dummy).map(function(obj, index){
					data.push(dummy[obj]);
				});

				vm.data = data.slice(0, 5);
			}
		}

})();